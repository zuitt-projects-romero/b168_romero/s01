<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures Activity</title>
</head>
<body>
	<h2>Full Address</h2>

	<p><?php echo getFullAddress('3F Caswnn BLDG., Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines'); ?></p>
	<p><?php echo getFullAddress('3F Enzo BLDG., Buendia Avenue', 'Makati City', 'Metro Manila', 'Philippines'); ?></p>

	<h2>Letter-Based Grading</h2>

	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>


</body>
</html>